package player

import "gitlab.com/Texan/Dominion/cards"

type Player struct {
	Name string
	Hand []cards.Card
}

func (p Player) VictoryPoints() int {
	points := 0
	for _, card := range p.Hand {
		if card.Type == "Victory" {
			points += card.Victory
		}
	}
	return points
}

func GardensPoints(cards []cards.Card) int {
	victoryCards := 0
	for _, card := range cards {
		if card.Type == "Victory" {
			victoryCards += 1
		}
	}
	return victoryCards / 4
}
