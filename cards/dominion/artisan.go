package dominion

import "gitlab.com/Texan/Dominion/cards"

type Artisan struct{}

func (a Artisan) Card() cards.Card {
	artisan := cards.Card{}
	artisan.Name = "Artisan"
	artisan.Cost = 6
	return artisan
}

func (a Artisan) Action(*[]cards.ICard) {
	// Gain a card to your hand costing up to $5.
	// Put a card from your hand onto your deck.

}

func (a Artisan) Gain() {}
