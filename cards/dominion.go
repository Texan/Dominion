package cards

type Dominion struct {
	Artisan     Card
	Bandit      Card
	Bureaucrat  Card
	Cellar      Card
	Chapel      Card
	CouncilRoom Card
	Festival    Card
	Gardens     Card
	Harbinger   Card
	Laboratory  Card
	Library     Card
	Market      Card
	Merchant    Card
	Militia     Card
	Mine        Card
	Moat        Card
	Moneylender Card
	Poacher     Card
	Remodel     Card
	Sentry      Card
	Smithy      Card
	ThroneRoom  Card
	Vassal      Card
	Village     Card
	Witch       Card
	Workshop    Card
}

func SetupDominion(players int) Dominion {
	dominion := Dominion{}

	extra := 0
	if players > 2 {
		extra = 4
	}

	dominion.Gardens = Card{Name: "Gardens", Cost: 4, Victory: 4, Count: 8 + extra + players*3, Type: "Victory"}

	return dominion
}

func PlayCard(card string) {
	switch card {
	case "Artisan":
	}
}
