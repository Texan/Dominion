package cards

type BaseCards struct {
	Copper Card
	Silver Card
	Gold   Card

	Estate   Card
	Duchy    Card
	Provence Card
	Curse    Card
}

func SetupBaseCards(players int) BaseCards {
	baseCards := BaseCards{}

	baseCards.Copper = Card{Name: "Copper", Cost: 0, Value: 1, Count: 60, Type: "Treasure"}
	baseCards.Silver = Card{Name: "Silver", Cost: 3, Value: 2, Count: 40, Type: "Treasure"}
	baseCards.Gold = Card{Name: "Gold", Cost: 6, Value: 3, Count: 30, Type: "Treasure"}

	extra := 0
	if players > 2 {
		extra = 4
	}

	baseCards.Estate = Card{Name: "Estate", Cost: 2, Victory: 1, Count: 8 + extra + players*3, Type: "Victory"}
	baseCards.Duchy = Card{Name: "Duchy", Cost: 5, Victory: 3, Count: 8 + extra, Type: "Victory"}
	baseCards.Provence = Card{Name: "Provence", Cost: 8, Victory: 6, Count: 8 + extra, Type: "Victory"}
	baseCards.Curse = Card{Name: "Curse", Cost: 0, Victory: -1, Count: 10 * (players - 1), Type: "Curse"}

	return baseCards
}
