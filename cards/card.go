package cards

type Card struct {
	Name    string
	Type    string
	Cost    int
	Cards   int
	Actions int
	Buys    int
	Value   int
	Victory int
	Count   int
}

type ICard interface {
	Card() Card
	Action(*[]ICard)
	Gain()
}
