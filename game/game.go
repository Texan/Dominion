package game

import (
	"gitlab.com/Texan/Dominion/cards"
	"gitlab.com/Texan/Dominion/player"
)

func NewGame() {
	baseCards := cards.SetupBaseCards(2)
	playerOne := player.Player{Name: "PlayerOne", Hand: []cards.Card{baseCards.Copper, baseCards.Estate}}
	playerOne.Hand[0].Count = 7
	playerOne.Hand[1].Count = 3
	playerTwo := player.Player{Name: "PlayerTwo", Hand: []cards.Card{baseCards.Copper, baseCards.Estate}}
	playerTwo.Hand[0].Count = 7
	playerTwo.Hand[1].Count = 3
}
