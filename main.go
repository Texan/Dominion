package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	fmt.Printf("%s\n\n", "Welcome to Dominion!")
	fmt.Printf("%s\n", "What would you like to do?")
	fmt.Printf("%s\n", "1. New Game")
	fmt.Printf("%s\n", "2. Quit")

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		switch scanner.Text() {
		case "1":
			fmt.Printf("%s\n", "New game!")
		case "2":
			os.Exit(0)
		default:
			fmt.Printf("%s\n", "Invalid option.")
		}
	}
}
